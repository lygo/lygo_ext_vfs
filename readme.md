# LyGo VFS #

![](icon.png)

Virtual File System implementation.

Support:
 - OS: Operating System file system
 - SFTP: SFTP protocol

**GOOGLE DRIVE**

Dependencies:
```
go get -u google.golang.org/api/drive/v3
go get -u golang.org/x/oauth2/google
```

## How to Use ##

To use just call:

```
go get -u bitbucket.org/lygo/lygo_ext_vfs@v0.1.10
```

### Versioning ###

Sources are versioned using git tags:

```
git tag v0.1.10
git push origin v0.1.10
```

