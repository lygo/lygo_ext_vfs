package vfsbackends

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_ext_vfs/vfscommons"
	"os"
	"strings"
)

//----------------------------------------------------------------------------------------------------------------------
//	VfsOS
//----------------------------------------------------------------------------------------------------------------------

type VfsOS struct {
	settings *vfscommons.VfsSettings

	user string

	startDir string
	curDir   string
}

func NewVfsOS(settings *vfscommons.VfsSettings) (instance *VfsOS, err error) {
	instance = new(VfsOS)
	instance.settings = settings

	err = instance.init()

	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *VfsOS) String() string {
	return lygo_json.Stringify(instance.settings)
}

func (instance *VfsOS) Close() {

}

func (instance *VfsOS) Path() string {
	return instance.curDir
}

func (instance *VfsOS) Cd(path string) (bool, error) {
	instance.curDir = vfscommons.Absolutize(instance.curDir, path)
	return lygo_paths.Exists(instance.curDir)
}

func (instance *VfsOS) Stat(path string) (*vfscommons.VfsFile, error) {
	fullPath := vfscommons.Absolutize(instance.curDir, path)
	info, err := os.Stat(fullPath)
	if nil != err {
		return nil, err
	}
	return vfscommons.NewVfsFile(fullPath, instance.curDir, info), nil

}

func (instance *VfsOS) Exists(path string) (bool, error) {
	return lygo_paths.Exists(vfscommons.Absolutize(instance.curDir, path))
}

func (instance *VfsOS) List(dir string) ([]*vfscommons.VfsFile, error) {
	response := make([]*vfscommons.VfsFile, 0)
	dir = vfscommons.Absolutize(instance.curDir, dir)
	list, err := os.ReadDir(dir)
	if nil == err {
		for _, entry := range list {
			fullPath := lygo_paths.Concat(dir, entry.Name())
			info, err := instance.Stat(fullPath)
			if nil == err {
				response = append(response, info)
			}
		}
	}
	return response, err
}

func (instance *VfsOS) Read(source string) ([]byte, error) {
	return lygo_io.ReadBytesFromFile(vfscommons.Absolutize(instance.curDir, source))
}

func (instance *VfsOS) Write(data []byte, target string) (int, error) {
	target = vfscommons.Absolutize(instance.curDir, target)
	err := lygo_paths.Mkdir(target)
	if nil != err {
		return 0, err
	}
	return lygo_io.WriteBytesToFile(data, target)
}

func (instance *VfsOS) Download(source, target string) ([]byte, error) {
	data, err := instance.Read(vfscommons.Absolutize(instance.curDir, source))
	if nil != err {
		return nil, err
	}
	_, err = lygo_io.WriteBytesToFile(data, vfscommons.Absolutize(instance.curDir, target))
	if nil != err {
		return nil, err
	}
	return data, nil
}

func (instance *VfsOS) Remove(source string) error {
	return lygo_io.Remove(vfscommons.Absolutize(instance.curDir, source))
}

func (instance *VfsOS) MkDir(path string) error {
	return lygo_paths.Mkdir(vfscommons.Absolutize(instance.curDir, path))
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *VfsOS) init() error {
	if nil != instance.settings {
		_, root := instance.settings.SplitLocation()

		if strings.HasPrefix(root, ".") {
			userHome, err := lygo_paths.UserHomeDir()
			if nil != err {
				return err
			}
			instance.startDir = lygo_paths.Concat(userHome, root)
		} else {
			// absolute path (user dir not used)
			instance.startDir = root
		}
		instance.curDir = instance.startDir
		if b, err := lygo_paths.Exists(instance.curDir); !b {
			return err
		}
		return nil
	}
	return vfscommons.MissingConfigurationError
}
