package vfsbackends

import (
	"bitbucket.org/lygo/lygo_ext_vfs/vfscommons"
)

// GOOGLE DRIVE SUPPORT
// https://developers.google.com/drive/api/v3/quickstart/go

//----------------------------------------------------------------------------------------------------------------------
//	VfsGD
//----------------------------------------------------------------------------------------------------------------------

type VfsGD struct {
	settings *vfscommons.VfsSettings

	user string

	startDir string
	curDir   string
}



