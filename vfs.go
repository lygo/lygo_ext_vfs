package lygo_ext_vfs

import (
	"bitbucket.org/lygo/lygo_commons/lygo_errors"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_ext_vfs/vfsbackends"
	"bitbucket.org/lygo/lygo_ext_vfs/vfscommons"
)

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func NewVfs(args ...interface{}) (vfscommons.IVfs, error) {
	switch len(args) {
	case 1:
		arg := args[0]
		if s, b := arg.(string); b {
			settings, err := vfscommons.LoadVfsSettings(s)
			if nil != err {
				return nil, err
			}
			return newVfs(settings)
		} else if c, b := arg.(vfscommons.VfsSettings); b {
			return newVfs(&c)
		} else if p, b := arg.(*vfscommons.VfsSettings); b {
			return newVfs(p)
		} else {
			s = lygo_json.Stringify(arg)
			settings, err := vfscommons.ParseVfsSettings(s)
			if nil != err {
				return nil, err
			}
			return newVfs(settings)
		}
	default:
		return nil, vfscommons.MismatchConfigurationError
	}
	return nil, vfscommons.MismatchConfigurationError
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func newVfs(settings *vfscommons.VfsSettings) (vfscommons.IVfs, error) {
	schema := settings.Schema()
	switch schema {
	case vfscommons.SchemaOS:
		return vfsbackends.NewVfsOS(settings)
	case vfscommons.SchemaSFTP:
		return vfsbackends.NewVfsSftp(settings)
	case vfscommons.SchemaFTP:
		return vfsbackends.NewVfsFtp(settings)
	default:
		return nil, lygo_errors.Prefix(vfscommons.UnsupportedSchemaError, schema + ": ")
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	i n i t
//----------------------------------------------------------------------------------------------------------------------

func init() {

}
