module bitbucket.org/lygo/lygo_ext_vfs

go 1.15

require (
	bitbucket.org/lygo/lygo_commons v0.1.120
	github.com/google/uuid v1.3.0 // indirect
	github.com/jlaffaye/ftp v0.0.0-20210307004419-5d4190119067
	github.com/pkg/sftp v1.13.3
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5
	golang.org/x/sys v0.0.0-20210910150752-751e447fb3d0 // indirect
)
