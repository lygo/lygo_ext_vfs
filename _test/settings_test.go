package _test

import (
	"bitbucket.org/lygo/lygo_ext_vfs/commons"
	"fmt"
	"testing"
)

func TestSettings(t *testing.T) {

	settings, err := vfscommons.LoadVfsSettings("./settings_os.json")
	if nil!=err{
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(settings)
}
