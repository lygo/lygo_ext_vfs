package vfscommons

import (
	"bitbucket.org/lygo/lygo_commons/lygo_conv"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"strings"
)

func ErrorContains(err error, text string) bool {
	lowErr := strings.ToLower(err.Error())
	lowTxt := strings.ToLower(text)
	return strings.Index(lowErr, lowTxt)>-1
}

func IsFile(path string) bool{
	return len(lygo_paths.Extension(path))>0
}

func Absolutize(root, path string) string {
	if strings.HasPrefix(path, ".") {
		return lygo_paths.Concat(root, path)
	}
	return path
}

func Relativize(root, path string) string {
	if !strings.HasPrefix(path, ".") {
		response :=  strings.Replace(path, root, "", 1)
		if strings.HasPrefix(response, "/"){
			response = "." + response
		} else {
			response = "./" + response
		}
		return response
	}
	return path
}

func ReadKey(pathOrKey string) ([]byte, error) {
	if len(pathOrKey) == 0 {
		return []byte{}, nil
	}
	if b, err := lygo_paths.IsFile(pathOrKey); b && nil == err {
		data, err := lygo_io.ReadBytesFromFile(pathOrKey)
		if nil != err {
			return []byte{}, err
		}
		return data, nil
	}
	return []byte(pathOrKey), nil
}

func SplitHost(settings *VfsSettings) (host string, port int) {
	port = 22
	_, full := settings.SplitLocation()
	tokens := strings.Split(full, ":")
	switch len(tokens) {
	case 1:
		host = tokens[0]
	case 2:
		host = tokens[0]
		port = lygo_conv.ToInt(tokens[1])
	}
	return
}
